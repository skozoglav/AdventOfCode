﻿using AoC.Common;

namespace Aoc.SolutionsRunner
{
    internal static class SolutionsRepository
    {
        private static readonly List<ISolution> All2015Solutions = new()
        {
            new AoC_2015.Solutions.Day01(),
            new AoC_2015.Solutions.Day02(),
            new AoC_2015.Solutions.Day03(),
            new AoC_2015.Solutions.Day04(),
            new AoC_2015.Solutions.Day05(),
            new AoC_2015.Solutions.Day06(),
            new AoC_2015.Solutions.Day07(),
            new AoC_2015.Solutions.Day08(),
        };

        // Commented out solutions are not working after the project transfer and I'm currently to lazy to fix them.
        // Probably most of the issues are with the line endings in the input files.
        private static readonly List<ISolution> All2020Solutions = new()
        {
            new AoC_2020.Solutions.Day01(),
            new AoC_2020.Solutions.Day02(),
            new AoC_2020.Solutions.Day03(),
            new AoC_2020.Solutions.Day04(),
            new AoC_2020.Solutions.Day05(),
            new AoC_2020.Solutions.Day06(),
            new AoC_2020.Solutions.Day07(),
            new AoC_2020.Solutions.Day08(),
            new AoC_2020.Solutions.Day09(),
            new AoC_2020.Solutions.Day10(),
            new AoC_2020.Solutions.Day11(),
            new AoC_2020.Solutions.Day12(),
            new AoC_2020.Solutions.Day13(),
            new AoC_2020.Solutions.Day14(),
            new AoC_2020.Solutions.Day15(),
            //new AoC_2020.Solutions.Day16(),
            new AoC_2020.Solutions.Day17(),
            new AoC_2020.Solutions.Day18(),
            new AoC_2020.Solutions.Day19(),
            //new AoC_2020.Solutions.Day20(),
            new AoC_2020.Solutions.Day21(),
            //new AoC_2020.Solutions.Day22(),
            new AoC_2020.Solutions.Day23(),
            new AoC_2020.Solutions.Day24(),
            new AoC_2020.Solutions.Day25(),
        };

        private static readonly List<ISolution> All2021Solutions = new()
        {
            new AoC_2021.Solutions.Day01(),
            new AoC_2021.Solutions.Day02(),
            new AoC_2021.Solutions.Day03(),
            new AoC_2021.Solutions.Day04(),
            new AoC_2021.Solutions.Day05(),
            new AoC_2021.Solutions.Day06(),
            new AoC_2021.Solutions.Day07(),
            new AoC_2021.Solutions.Day08(),
            new AoC_2021.Solutions.Day09(),
            new AoC_2021.Solutions.Day10(),
            new AoC_2021.Solutions.Day11(),
            new AoC_2021.Solutions.Day12(),
            new AoC_2021.Solutions.Day13(),
            new AoC_2021.Solutions.Day14(),
            new AoC_2021.Solutions.Day15(),
            //new AoC_2021.Solutions.Day16(),
            //new AoC_2021.Solutions.Day17(),
            //new AoC_2021.Solutions.Day18(),
            //new AoC_2021.Solutions.Day19(),
            //new AoC_2021.Solutions.Day20(),
            //new AoC_2021.Solutions.Day21(),
            //new AoC_2021.Solutions.Day22(),
            //new AoC_2021.Solutions.Day23(),
            //new AoC_2021.Solutions.Day24(),
            //new AoC_2021.Solutions.Day25(),
        };

        public static ISolution GetSolverByYearAndDay(int year, int day)
        {
            if (year == 2015)
            {
                return All2015Solutions[day];
            }
            else if (year == 2020)
            {
                return All2020Solutions[day];
            }
            else if (year == 2021)
            {
                return All2021Solutions[day];
            }

            throw new NotSupportedException();
        }

        public static List<ISolution> GetAllSolversByYear(int year)
        {
            if (year == 2015)
            {
                return All2015Solutions;
            }
            else if (year == 2020)
            {
                return All2020Solutions;
            }
            else if (year == 2021)
            {
                return All2021Solutions;
            }

            throw new NotSupportedException();
        }

        public static List<ISolution> GetAllSolvers()
        {
            return All2015Solutions.Concat(All2020Solutions).Concat(All2021Solutions).ToList();
        }
    }
}
