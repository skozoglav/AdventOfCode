﻿using AoC.Common;
using AoC_2021.Solutions;

namespace Aoc.SolutionsRunner
{
    /// <summary>
    /// Runner for the AoC solutions.
    /// </summary>
    public class ProblemSolver
    {
        public static void Main()
        {
            //ISolution solver = SolutionsRepository.GetAllSolversByYear(2021).Last();
            //solver.Solve(true);

            new Day15().Solve(true);
             
        }
    }
}
