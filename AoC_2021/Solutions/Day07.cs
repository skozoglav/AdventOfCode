﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 7.
    /// </summary>
    public class Day07 : BaseSolution
    {
        private readonly List<int> input;

        public Day07() : base(2021, 7)
        {
            this.input = InputParser.LineToIntArray(FilePath).ToList();
        }

        /// <inheritdoc/>
        public override string GetPartOne() =>
            Enumerable.Range(this.input.Min(), this.input.Max() - this.input.Min())
                      .Min((i) => this.input.Select(num => Math.Abs(num - i)).Sum()).ToString();


        /// <inheritdoc/>
        public override string GetPartTwo() =>
            Enumerable.Range(this.input.Min(), this.input.Max() - this.input.Min())
                      .Min((i) => this.input.Select(num => Math.Abs(num - i)).Select(dis => dis * (dis + 1) / 2).Sum()).ToString();
    }
}
