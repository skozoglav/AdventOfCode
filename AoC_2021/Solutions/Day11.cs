﻿using AoC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 11.
    /// </summary>
    public class Day11 : BaseSolution
    {
        private readonly int[][] input;

        public Day11() : base(2021, 11)
        {
            this.input = File.ReadAllLines(FilePath)
                .Select(line => line.Select(c => c - '0').ToArray())
                .ToArray();
        }

        /// <inheritdoc/>
        public override string GetPartOne() => Enumerable.Range(0, 100).Sum(i => Step()).ToString();

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            int result = 101;
            while (Step() != 100) result+=1;

            return result.ToString();
        }

        private IEnumerable<(int y, int x)> Adjacent(int y, int x)
        {
            if (x > 0) yield return (y, x - 1);
            if (y > 0) yield return (y - 1, x);
            if (x < this.input[y].Length - 1) yield return (y, x + 1);
            if (y < this.input.Length - 1) yield return (y + 1, x);

            if (x > 0 && y > 0) yield return (y - 1, x - 1);
            if (x > 0 && y < this.input.Length - 1) yield return (y + 1, x - 1);
            if (x < this.input[y].Length - 1 && y > 0) yield return (y - 1, x + 1);
            if (x < this.input[y].Length - 1 && y < this.input.Length - 1) yield return (y + 1, x + 1);
        }

        private int Increment(int y, int x, HashSet<(int y, int x)> flashed)
        {
            if (flashed.Contains((y, x))) return 0;
            this.input[y][x]++;
            if (this.input[y][x] <= 9) return 0;

            this.input[y][x] = 0;
            flashed.Add((y, x));
            return 1 + Adjacent(y, x).Sum(a => Increment(a.y, a.x, flashed));
        }

        private int Step()
        {
            HashSet<(int y, int x)>? flashed = new();
            for (int row = 0; row < this.input.Length; ++row)
            {
                for (int col = 0; col < this.input[row].Length; ++col)
                {
                    Increment(row, col, flashed);
                }
            }
            return flashed.Count;
        }
    }
}
