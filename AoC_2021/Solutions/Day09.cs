﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 9.
    /// </summary>
    public class Day09 : BaseSolution
    {
        private readonly char[][] grid;

        public Day09() : base(2021, 9)
        {
            this.grid = InputParser.FileToStringArray(FilePath).Select(l => l.ToArray()).ToArray();
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
            List<(int y, int x)> lows = new();
            for (int y = 0; y < this.grid.Length; y++)
            {
                for (int x = 0; x < this.grid[0].Length; x++)
                {
                    if (Adjacent(y, x).All(a => grid[a.y][a.x] > grid[y][x])) lows.Add((y, x));
                }
            }

            return (lows.Sum(a => Int32.Parse($"{grid[a.y][a.x]}")) + lows.Count).ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            List<(int y, int x)> lows = new();
            for (int y = 0; y < this.grid.Length; y++)
            {
                for (int x = 0; x < this.grid[0].Length; x++)
                {
                    if (Adjacent(y, x).All(a => grid[a.y][a.x] > grid[y][x])) lows.Add((y, x));
                }
            }

            HashSet<(int y, int x)> visited = new();
            IEnumerable<(int y, int x)> basin((int y, int x) low)
            {
                if (!visited.Contains(low) && grid[low.y][low.x] != '9')
                {
                    visited.Add(low);
                    yield return low;

                    foreach (var a in Adjacent(low.y, low.x))
                        foreach (var c in basin(a))
                            yield return c;
                }
            }
            var basins = lows.Select(l => basin(l).ToArray()).OrderByDescending(b => b.Length).ToArray();

            return (basins.Take(3).Aggregate(1L, (s, b) => s * b.Count())).ToString();
        }

        private IEnumerable<(int y, int x)> Adjacent(int y, int x)
        {
            if (x > 0) yield return (y, x - 1);
            if (x < grid[0].Length - 1) yield return (y, x + 1);
            if (y > 0) yield return (y - 1, x);
            if (y < grid.Length - 1) yield return (y + 1, x);
        }
    }
}
