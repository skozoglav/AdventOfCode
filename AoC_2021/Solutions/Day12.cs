﻿using AoC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 12.
    /// </summary>
    public class Day12 : BaseSolution
    {
        private readonly string[] input;

        public Day12() : base(2021, 12)
        {
            this.input = InputParser.FileToStringArray(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
            var edges = input.Select(s => s.Split('-'));
            var graph = edges.Union(edges.Select(e => e.Reverse().ToArray()))
                .ToLookup(e => e[0], e => e[1]);

            string[][] GetPaths(string vertex, string[] visited, int max)
            {
                var path = visited.Append(vertex).ToArray();
                if (vertex == "end")
                    return new string[][] { path };

                var lower = path.Where(v => char.IsLower(v[0])).ToArray();
                var exclude = lower.GroupBy(v => v).Any(g => g.Count() == max) ? lower
                    : new string[] { "start" };
                return graph[vertex].Except(exclude)
                    .SelectMany(node => GetPaths(node, path, max))
                    .ToArray();
            }

            int paths(int max) => GetPaths("start", new string[0], max).Count();

            return paths(1).ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            var edges = input.Select(s => s.Split('-'));
            var graph = edges.Union(edges.Select(e => e.Reverse().ToArray()))
                .ToLookup(e => e[0], e => e[1]);

            string[][] GetPaths(string vertex, string[] visited, int max)
            {
                var path = visited.Append(vertex).ToArray();
                if (vertex == "end")
                    return new string[][] { path };

                var lower = path.Where(v => char.IsLower(v[0])).ToArray();
                var exclude = lower.GroupBy(v => v).Any(g => g.Count() == max) ? lower
                    : new string[] { "start" };
                return graph[vertex].Except(exclude)
                    .SelectMany(node => GetPaths(node, path, max))
                    .ToArray();
            }

            int paths(int max) => GetPaths("start", new string[0], max).Count();

            return paths(2).ToString();
        }

        
    }
}
