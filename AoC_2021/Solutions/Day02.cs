﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 2.
    /// </summary>
    public class Day02 : BaseSolution
    {
        private readonly List<KeyValuePair<string, int>> input;

        public Day02() : base(2021, 2)
        {
            this.input = InputParser.FileToStringIntPairs(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
            int horizontal = 0, depth = 0;
            foreach ((string direction, int unit) in this.input)
            {
                _ = direction switch
                {
                    "forward" => horizontal += unit,
                    "up" => depth -= unit,
                    "down" => depth += unit,
                    _ => throw new ArgumentException(),
                };
            }

            return (horizontal * depth).ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            int horizontal = 0, depth = 0, aim = 0;
            foreach ((string direction, int unit) in this.input)
            {
                _ = direction switch
                {
                    "forward" => (horizontal += unit, depth += aim * unit),
                    "up" => (0, aim -= unit),
                    "down" => (0, aim += unit),
                    _ => throw new ArgumentException(),
                };
            }

            return (horizontal * depth).ToString();
        }
    }
}
