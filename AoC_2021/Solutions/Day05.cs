﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 5.
    /// </summary>
    public class Day05 : BaseSolution
    {
        private readonly List<Vent> vents;

        public Day05() : base(2021, 5)
        {
            this.vents = InputParser.FileToStringArray(FilePath)
                .Select(l => Regex.Match(l, @"(\d+),(\d+) -> (\d+),(\d+)"))
                .Select(m => new Vent()
                {
                    X1 = Convert.ToInt32(m.Groups[1].Value),
                    Y1 = Convert.ToInt32(m.Groups[2].Value),
                    X2 = Convert.ToInt32(m.Groups[3].Value),
                    Y2 = Convert.ToInt32(m.Groups[4].Value)
                })
                .ToList();
        }

        /// <inheritdoc/>
        public override string GetPartOne() => ExecutePart(this.vents);

        /// <inheritdoc/>
        public override string GetPartTwo() => ExecutePart(this.vents, false);

        private static string ExecutePart(List<Vent> vents, bool skipDiagonals = true)
        {
            Dictionary<(int x, int y), int> visited = new();
            foreach (Vent vent in vents)
            {
                if (skipDiagonals && vent.X1 != vent.X2 && vent.Y1 != vent.Y2) continue;

                var xDir = Math.Sign(vent.X2 - vent.X1);
                var yDir = Math.Sign(vent.Y2 - vent.Y1);
                for (int x = vent.X1, y = vent.Y1; x != (vent.X2 + xDir) || y != (vent.Y2 + yDir); x += xDir, y += yDir)
                    visited[(x, y)] = visited.GetValueOrDefault((x, y)) + 1;
            }

            return visited.Where(kvp => kvp.Value > 1).Count().ToString();
        }

        private class Vent
        {
            public int X1 { get; init; }
            public int Y1 { get; init; }
            public int X2 { get; init; }
            public int Y2 { get; init; }
        }
    }
}
