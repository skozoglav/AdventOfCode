﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 4.
    /// </summary>
    public class Day04 : BaseSolution
    {
        private readonly int[] numbers;
        private readonly HashSet<Board> boards;

        public Day04() : base(2021, 4)
        {
            string[] input = File.ReadAllText(FilePath).Split("\n\n");
            this.numbers = input[0].Split(',').Select(x => Int32.Parse(x)).ToArray();

            this.boards = new();
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i].EndsWith("\n"))
                {
                    input[i] = input[i][0..^2];
                }
                int[][] board = input[i].Replace("  ", " ").Split('\n').Select(x => x.Trim().Split(' ').Select(x => Int32.Parse(x)).ToArray()).ToArray();

                this.boards.Add(new Board() { BoardLayout = board });
            }
        }

        /// <inheritdoc/>
        public override string GetPartOne() => ExecutePart(this.boards, this.numbers);


        /// <inheritdoc/>
        public override string GetPartTwo() => ExecutePart(this.boards, this.numbers, true);

        private static string ExecutePart(HashSet<Board> boardsSet, int[] numbers, bool isPartTwo = false)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                foreach (Board board in boardsSet)
                {
                    board.MarkNumber(numbers[i]);
                }

                if (isPartTwo && boardsSet.Count > 1)
                {
                    boardsSet.RemoveWhere(board => board.IsBingo());
                }

                Board? bingo = boardsSet.FirstOrDefault(board => board.IsBingo());
                if (bingo is not null)
                {
                    int sum = bingo.GetUnmarkedNumbersSum(numbers[0..(i + 1)]);
                    return (sum * numbers[i]).ToString();
                }
            }
            return String.Empty;
        }

        private class Board
        {
            internal int[][] BoardLayout { get; init; }

            internal Dictionary<int, int> BoardRowMarks { get; private set; }

            internal Dictionary<int, int> BoardColumnMarks { get; private set; }

            internal void MarkNumber(int number)
            {
                if (BoardRowMarks is null)
                {
                    BoardRowMarks = new();
                }

                if (BoardColumnMarks is null)
                {
                    BoardColumnMarks = new();
                }

                for (int i = 0; i < BoardLayout.Length; i++)
                {
                    for (int j = 0; j < BoardLayout[0].Length; j++)
                    {
                        if (BoardLayout[i][j] == number)
                        {
                            if (!BoardRowMarks.TryAdd(i, 1))
                            {
                                BoardRowMarks[i] += 1;
                            }
                            if (!BoardColumnMarks.TryAdd(j, 1))
                            {
                                BoardColumnMarks[j] += 1;
                            }
                        }
                    }
                }
            }

            internal bool IsBingo()
            {
                foreach (KeyValuePair<int, int> marks in BoardRowMarks)
                {
                    if (marks.Value >= 5)
                    {
                        return true;
                    }
                }
                foreach (KeyValuePair<int, int> marks in BoardColumnMarks)
                {
                    if (marks.Value >= 5)
                    {
                        return true;
                    }
                }
                return false;
            }

            internal int GetUnmarkedNumbersSum(int[] numbers)
            {
                int sum = 0;

                for (int i = 0; i < BoardLayout.Length; i++)
                {
                    for (int j = 0; j < BoardLayout[0].Length; j++)
                    {

                        if (!numbers.Contains(BoardLayout[i][j]))
                        {
                            sum += BoardLayout[i][j];
                        }
                    }
                }

                return sum;
            }
        }
    }
}
