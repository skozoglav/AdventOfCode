﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 6.
    /// </summary>
    public class Day06 : BaseSolution
    {
        private readonly int[] input;

        public Day06() : base(2021, 6)
        {
            this.input = InputParser.LineToIntArray(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne() => ExecutePart(80);

        /// <inheritdoc/>
        public override string GetPartTwo() => ExecutePart(256);

        private string ExecutePart(int iterations)
        {
            Dictionary<int, long> fishes = GetDefaultDict();
            foreach (int i in this.input)
            {
                fishes[i] += 1;
            }

            for (int i = 0; i < iterations; i++)
            {
                Dictionary<int, long> temp = GetDefaultDict();
                foreach ((int age, long count) in fishes)
                {
                    if (age > 0)
                    {
                        temp[age - 1] += count;
                    }
                    else
                    {
                        temp[6] += count;
                        temp[8] += count;
                    }
                }
                fishes = temp;
            }

            return fishes.Values.Sum().ToString();
        }

        private static Dictionary<int, long> GetDefaultDict() =>
            new()
            {
                { 0, 0 },
                { 1, 0 },
                { 2, 0 },
                { 3, 0 },
                { 4, 0 },
                { 5, 0 },
                { 6, 0 },
                { 7, 0 },
                { 8, 0 },
            };
    }
}
