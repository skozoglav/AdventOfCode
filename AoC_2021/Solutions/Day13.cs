﻿using AoC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 13.
    /// </summary>
    public class Day13 : BaseSolution
    {
        private HashSet<(int X, int Y)> dots;
        private readonly List<(string Axis, int Line)> foldInstructions;

        public Day13() : base(2021, 13)
        {
            string[] input = File.ReadAllText(FilePath).Split("\n\n");

            dots = input[0].Split("\n").Select(x => (X: int.Parse(x.Split(',')[0]), Y: int.Parse(x.Split(',')[1]))).ToHashSet();
            foldInstructions = input[1].Replace("fold along ", "").Trim().Split("\n").Select(x => (x.Split("=")[0], int.Parse(x.Split("=")[1]))).ToList();
        }

        /// <inheritdoc/>
        public override string GetPartOne() => Fold(foldInstructions[0].Axis, foldInstructions[0].Line, dots).Count.ToString();

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            var paper = dots;
            foldInstructions.ForEach(x => paper = Fold(x.Axis, x.Line, paper));

            var maxX = paper.Max(x => x.X);
            var maxY = paper.Max(x => x.Y);

            for (var y = 0; y <= maxY; y++)
            {
                var buffer = string.Empty;
                for (var x = 0; x <= maxX; x++)
                {
                    buffer += paper.Contains((x, y)) ? "#" : " ";
                }
                Console.WriteLine(buffer);
            }

            return "Read the code above.";
        }

        private static HashSet<(int X, int Y)> Fold(string axis, int line, HashSet<(int X, int Y)> paper)
        {
            if (axis == "x")
            {
                paper.Where(x => x.X > line).ToList().ForEach(x =>
                {
                    var newX = x.X - (x.X - line) * 2;
                    paper.Add((newX, x.Y));
                    paper.Remove(x);
                });
            }
            else
            {
                paper.Where(x => x.Y > line).ToList().ForEach(x =>
                {
                    var newY = x.Y - (x.Y - line) * 2;
                    paper.Add((x.X, newY));
                    paper.Remove(x);
                });
            }
            return paper;
        }
    }
}
