﻿using AoC.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 10.
    /// </summary>
    public class Day10 : BaseSolution
    {
		private readonly string[] input;

        public Day10() : base(2021, 10)
        {
			this.input = InputParser.FileToStringArray(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
			return this.input
			.Select(static l =>
			{
				var stack = new Stack<char>();
				foreach (var c in l)
				{
					if (c is '{' or '(' or '[' or '<')
						stack.Push(c);
					else
					{
						var o = stack.Pop();
						var v = (o, c) switch
						{
							('(', ')') => -1,
							(_, ')') => 3,

							('[', ']') => -1,
							(_, ']') => 57,

							('{', '}') => -1,
							(_, '}') => 1197,

							('<', '>') => -1,
							(_, '>') => 25137,
						};
						if (v != -1)
							return v;
					}
				}

				return 0;
			})
			.Sum()
			.ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
			var scores = input
			.Select(static l =>
			{
				var stack = new Stack<char>();
				foreach (var c in l)
				{
					if (c is '{' or '(' or '[' or '<')
						stack.Push(c);
					else
					{
						var o = stack.Pop();
						var v = (o, c) switch
						{
							('(', ')') => -1,
							(_, ')') => 3,

							('[', ']') => -1,
							(_, ']') => 57,

							('{', '}') => -1,
							(_, '}') => 1197,

							('<', '>') => -1,
							(_, '>') => 25137,
						};
						if (v != -1)
							return 0;
					}
				}

				return stack
					.Aggregate(
						0L,
						(i, c) => i * 5 + c switch
						{
							'(' => 1,
							'[' => 2,
							'{' => 3,
							'<' => 4,
						});
			})
			.Where(x => x != 0)
			.OrderBy(x => x)
			.ToList();
			return scores[scores.Count / 2].ToString();
		}
    }
}
