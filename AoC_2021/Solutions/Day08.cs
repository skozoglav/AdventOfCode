﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 8.
    /// </summary>
    public class Day08 : BaseSolution
    {
        private readonly List<LineInfo> input;

        public Day08() : base(2021, 8)
        {
            input = InputParser.FileToStringArray(FilePath).Select(LineInfo.Parse).ToList();
        }

        /// <inheritdoc/>
        public override string GetPartOne() =>
            input.SelectMany(i => i.Outputs).Count(x => x.Length is 7 or > 1 and < 5).ToString();

        /// <inheritdoc/>
        public override string GetPartTwo() => input.Sum(l => l.GetOutputValue()).ToString();

        private class LineInfo
        {
            public readonly string[] Outputs;

            private readonly HashSet<char> oneSegments;
            private readonly HashSet<char> fourSegments;

            private LineInfo(string[] outputs, HashSet<char> oneSegments, HashSet<char> fourSegments)
            {
                this.Outputs = outputs;
                this.oneSegments = oneSegments;
                this.fourSegments = fourSegments;
            }

            public static LineInfo Parse(string line)
            {
                string[] sections = line.Split(" | ", 2);
                string[] wiring = sections[0].Split(' ', 10);
                HashSet<char> one = wiring.First(w => w.Length == 2).ToHashSet();
                HashSet<char> four = wiring.First(w => w.Length == 4).ToHashSet();

                return new(sections[1].Split(' ', 4), one, four);
            }

            public int GetOutputValue()
            {
                int output = 0;
                foreach (string digitSegments in Outputs)
                {
                    int digitValue = digitSegments.Length switch
                    {
                        2 => 1,
                        3 => 7,
                        4 => 4,
                        5 => digitSegments.Count(fourSegments.Contains) == 2 ? 2
                            : digitSegments.Count(oneSegments.Contains) == 2 ? 3 : 5,
                        6 => digitSegments.Count(fourSegments.Contains) == 4 ? 9
                            : digitSegments.Count(oneSegments.Contains) == 2 ? 0 : 6,
                        7 => 8,
                        _ => throw new InvalidOperationException(),
                    };
                    output = output * 10 + digitValue;
                }

                return output;
            }
        }
    }
}
