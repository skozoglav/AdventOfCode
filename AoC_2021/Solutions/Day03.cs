﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 3.
    /// </summary>
    public class Day03 : BaseSolution
    {
        private readonly string[] input;

        public Day03() : base(2021, 3)
        {
            input = InputParser.FileToStringArray(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
            string gamma = "", epsilon = "";
            int size = this.input.Length / 2;

            for (int i = 0; i < this.input[0].Length; i++)
            {
                int counter = 0;
                foreach (string str in this.input)
                {
                    if (str[i] == '1')
                    {
                        counter++;
                    }
                }

                gamma += counter > size ? "1" : "0";
                epsilon += counter < size ? "1" : "0";
            }

            return (Convert.ToInt32(gamma, 2) * Convert.ToInt32(epsilon, 2)).ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            long oxygen = Convert.ToInt64(
                new String(FindRating(this.input.ToList(), 0, true)), 2);
            long co2 = Convert.ToInt64(
                new String(FindRating(this.input.ToList(), 0, false)), 2);

            return (oxygen * co2).ToString();
        }

        private char[] FindRating(List<string> list, int bit, bool criteria)
        {
            if (list.Count.Equals(1))
            {
                return list.First().ToCharArray();
            }

            List<string> onesContenders = list.FindAll(x => x[bit].Equals('1'));
            List<string> zerosContenders = list.FindAll(x => x[bit].Equals('0'));

            if (criteria)
            {
                if (onesContenders.Count >= zerosContenders.Count)
                {
                    return FindRating(onesContenders, ++bit, criteria);
                }

                return FindRating(zerosContenders, ++bit, criteria);
            }

            if (zerosContenders.Count <= onesContenders.Count)
            {
                return FindRating(zerosContenders, ++bit, criteria);
            }

            return FindRating(onesContenders, ++bit, criteria);
        }
    }
}
