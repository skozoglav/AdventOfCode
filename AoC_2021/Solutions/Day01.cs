﻿using AoC.Common;

namespace AoC_2021.Solutions
{
    /// <summary>
    /// Implementation of the AoC task Year 2021, Day 1.
    /// </summary>
    public class Day01 : BaseSolution
    {
        private readonly int[] input;

        public Day01() : base(2021, 1)
        {
            this.input = InputParser.FileToIntArray(FilePath);
        }

        /// <inheritdoc/>
        public override string GetPartOne()
        {
            int counter = 0;
            for (int i = 0; i < this.input.Length - 1; i++)
            {
                if (this.input[i] < this.input[i + 1])
                {
                    counter++;
                }
            }
            return counter.ToString();
        }

        /// <inheritdoc/>
        public override string GetPartTwo()
        {
            int counter = 0;
            for (int i = 0; i < this.input.Length - 3; i++)
            {
                if (this.input[i] < this.input[i + 3])
                {
                    counter++;
                }
            }
            return counter.ToString();
        }
    }
}
