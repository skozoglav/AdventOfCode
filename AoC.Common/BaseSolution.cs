﻿using System.Diagnostics;

namespace AoC.Common
{
    /// <summary>
    /// Base implementation of the <see cref="ISolution"/>.
    /// </summary>
    public abstract class BaseSolution : ISolution
    {
        /// <summary>
        /// Remembers the tasks year and day, and constructs the path to the input file.
        /// </summary>
        /// <param name="year">Year of the task.</param>
        /// <param name="day">Day of the task.</param>
#pragma warning disable CS8618, CS8601
        public BaseSolution(int year, int day)
        {
            Day = day;
            Year = year;
            Title = Resources.ResourceManager.GetString($"Year{Year}Day{Day:D2}");
            FilePath = $@".\Inputs\Year{year}Day{day:D2}.txt";
        }
#pragma warning restore CS8618, CS8601

        /// <inheritdoc/>
        public int Day { get; }

        /// <inheritdoc/>
        public int Year { get; }

        /// <inheritdoc/>
        public string Title { get; }

        /// <inheritdoc/>
        public string FilePath { get; }

        /// <inheritdoc/>
        public abstract string GetPartOne();

        /// <inheritdoc/>
        public abstract string GetPartTwo();

        /// <inheritdoc/>
        public void Solve(bool showDiagnostics = false)
        {
            Console.WriteLine($"Year {Year} - Day {Day} | {Title}\n");

            if (showDiagnostics)
            {
                Stopwatch sw = new();

                sw.Start();
                string answer1 = GetPartOne();
                sw.Stop();
                Console.WriteLine($"Solution Part 1: {answer1} | Time: {sw.Elapsed}");

                sw.Reset();
                sw.Start();
                string answer2 = GetPartTwo();
                sw.Stop();
                Console.WriteLine($"Solution Part 2: {answer2} | Time: {sw.Elapsed}");
            }
            else
            {
                Console.WriteLine($"Solution Part 1: {GetPartOne()}");
                Console.WriteLine($"Solution Part 2: {GetPartTwo()}");
            }

            Console.WriteLine();
        }
    }
}
