﻿namespace AoC.Common
{
    /// <summary>
    /// An interface for the AoC solution.
    /// </summary>
    public interface ISolution
    {
        /// <summary>
        /// Day of the AoC solution.
        /// </summary>
        int Day { get; }

        /// <summary>
        /// Year of the AoC solution.
        /// </summary>
        int Year { get; }

        /// <summary>
        /// Title of the AoC task.
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Path to the input file of the AoC task.
        /// </summary>
        string FilePath { get; }

        /// <summary>
        /// Implementation of the first part of the task.
        /// </summary>
        /// <returns>Solution for the first part of the task.</returns>
        string GetPartOne();

        /// <summary>
        /// Implementation of the second part of the task.
        /// </summary>
        /// <returns>Solution for the second part of the task.</returns>
        string GetPartTwo();

        /// <summary>
        /// Solves both parts of the task and prints the solution.
        /// </summary>
        /// <param name="showDiagnostics">True if you want to show execution time.</param>
        void Solve(bool showDiagnostics = false);
    }
}
