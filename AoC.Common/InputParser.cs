﻿namespace AoC.Common
{
    /// <summary>
    /// Helper class that parses the input to commonly used data types.
    /// </summary>
    public static class InputParser
    {
        /// <summary>
        /// Converts input to string array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static string[] FileToStringArray(string filePath) => File.ReadAllLines(filePath);

        /// <summary>
        /// Converts input to char array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static char[] FileToCharArray(string filePath) => File.ReadAllText(filePath).ToCharArray();

        /// <summary>
        /// Converts input to int array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static int[] FileToIntArray(string filePath) => Array.ConvertAll(FileToStringArray(filePath), s => Int32.Parse(s));

        /// <summary>
        /// Converts onliner input to int array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static int[] LineToIntArray(string filePath) => Array.ConvertAll(File.ReadAllText(filePath).Split(','), s => Int32.Parse(s));

        /// <summary>
        /// Converts input to long array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static long[] FileToLongArray(string filePath) => Array.ConvertAll(FileToStringArray(filePath), s => Int64.Parse(s));

        /// <summary>
        /// Converts input to string array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static List<KeyValuePair<string, int>> FileToStringIntPairs(string filePath)
        {
            List<KeyValuePair<string, int>> pairs = new();

            string[] lines = File.ReadAllLines(filePath);
            foreach (string line in lines)
            {
                string[] values = line.Split();
                pairs.Add(new(values[0], Int32.Parse(values[1])));
            }

            return pairs;
        }

        /// <summary>
        /// Converts each character in each line to int and combines it to an array.
        /// </summary>
        /// <param name="filePath">Path to the input file.</param>
        public static int[][] FileToInt2DArray(string filePath)
        {
            string[] file = FileToStringArray(filePath);
            int[][] array = new int[file.Length][];

            for (int i = 0; i < file.Length; i++)
            {
                array[i] = new int[file[i].Length];
                for (int j = 0; j < file[0].Length; j++)
                {
                    array[i][j] = (int)Char.GetNumericValue(file[i][j]);
                }
            }

            return array;
        }
    }
}
