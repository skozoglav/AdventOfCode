﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day12 : BaseSolution
    {
        private readonly (char, int)[] input;

        public Day12() : base(2020, 12)
        {
            input = File.ReadAllLines(FilePath)
                        .Select(x => (x[0], int.Parse(x[1..]))).ToArray();
        }

        public override string GetPartOne()
        {
            var currentPosition = (x: 0, y: 0);
            var currentDirection = NavigationDirection.E;

            foreach ((char instruction, int value) in input)
            {
                switch (instruction)
                {
                    case 'N': currentPosition = MoveDirection(currentPosition, NavigationDirection.N, value); break;
                    case 'S': currentPosition = MoveDirection(currentPosition, NavigationDirection.S, value); break;
                    case 'E': currentPosition = MoveDirection(currentPosition, NavigationDirection.E, value); break;
                    case 'W': currentPosition = MoveDirection(currentPosition, NavigationDirection.W, value); break;
                    case 'F': currentPosition = MoveDirection(currentPosition, currentDirection, value); break;
                    case 'L': currentDirection = (NavigationDirection)(((int)currentDirection - value + 360) % 360); break;
                    case 'R': currentDirection = (NavigationDirection)(((int)currentDirection + value) % 360); break;
                }
            }

            return ManhattanDistance((0, 0), currentPosition).ToString();
        }

        public override string GetPartTwo()
        {
            var shipPosition = (x: 0, y: 0);
            var wayPoint = (x: 10, y: 1);

            foreach ((char instruction, int value) in input)
            {
                switch (instruction)
                {
                    case 'N': wayPoint = MoveDirection(wayPoint, NavigationDirection.N, value); break;
                    case 'S': wayPoint = MoveDirection(wayPoint, NavigationDirection.S, value); break;
                    case 'E': wayPoint = MoveDirection(wayPoint, NavigationDirection.E, value); break;
                    case 'W': wayPoint = MoveDirection(wayPoint, NavigationDirection.W, value); break;
                    case 'F': shipPosition = Add(shipPosition, (value * wayPoint.x, value * wayPoint.y)); break;
                    case 'L':
                        switch (value)
                        {
                            case 90: wayPoint = (-wayPoint.y, wayPoint.x); break;
                            case 180: wayPoint = (-wayPoint.x, -wayPoint.y); break;
                            case 270: wayPoint = (wayPoint.y, -wayPoint.x); break;
                        }
                        break;
                    case 'R':
                        switch (value)
                        {
                            case 270: wayPoint = (-wayPoint.y, wayPoint.x); break;
                            case 180: wayPoint = (-wayPoint.x, -wayPoint.y); break;
                            case 90: wayPoint = (wayPoint.y, -wayPoint.x); break;
                        }
                        break;
                }
            }

            return ManhattanDistance((0, 0), shipPosition).ToString();
        }

        #region Navigation structures

        private enum NavigationDirection
        {
            N = 0,
            NE = 45,
            E = 90,
            SE = 135,
            S = 180,
            SW = 225,
            W = 270,
            NW = 315
        }

        private (int x, int y) MoveDirection((int x, int y) start, NavigationDirection direction, int distance = 1)
        {
            return (direction) switch
            {
                NavigationDirection.N => Add(start, (0, distance)),
                NavigationDirection.NE => Add(start, (distance, distance)),
                NavigationDirection.E => Add(start, (distance, 0)),
                NavigationDirection.SE => Add(start, (distance, -distance)),
                NavigationDirection.S => Add(start, (0, -distance)),
                NavigationDirection.SW => Add(start, (-distance, -distance)),
                NavigationDirection.W => Add(start, (-distance, 0)),
                NavigationDirection.NW => Add(start, (-distance, distance)),
                _ => throw new ArgumentException("Direction is not valid.", nameof(direction))
            };
        }

        #endregion

        #region Helper methods

        private (int x, int y) Add((int x, int y) a, (int x, int y) b) => (a.x + b.x, a.y + b.y);

        private int ManhattanDistance((int x, int y) a, (int x, int y) b) => Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);

        #endregion
    }
}
