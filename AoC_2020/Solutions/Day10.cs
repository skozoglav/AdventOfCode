﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day10 : BaseSolution
    {
        private readonly int[] input;
        private readonly Dictionary<(int, int), long> cache;

        public Day10() : base(2020, 10)
        {
            input = File.ReadAllLines(FilePath).Select(int.Parse).OrderBy(j => j).ToArray();
            cache = new Dictionary<(int, int), long>();
        }

        public override string GetPartOne()
        {
            int ones = Math.Abs(input[0] - 2);
            int threes = 1;

            for (int i = 0; i < input.Length - 1; i++)
            {
                if (input[i + 1] - input[i] == 1) ones++;
                else if (input[i + 1] - input[i] == 3) threes++;
            }

            return (threes * ones).ToString();

        }

        public override string GetPartTwo() =>
            CountWays(input, 0, input.Max() + 3).ToString();

        #region Helper methods

        private long CountWays(int[] input, int start, int goal)
        {
            var k = (input.Length, start);
            if (cache.ContainsKey(k))
            {
                return cache[k];
            }

            long conn = 0;
            if (goal - start <= 3)
            {
                conn += 1;
            }

            if (input.Length == 0)
            {
                return conn;
            }

            if (input[0] - start <= 3)
            {
                conn += CountWays(input.Skip(1).ToArray(), input[0], goal);
            }

            conn += CountWays(input.Skip(1).ToArray(), start, goal);
            cache[k] = conn;
            return conn;
        }

        #endregion

    }
}
