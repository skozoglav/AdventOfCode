﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day25 : BaseSolution
    {
        private readonly long[] input;

        public Day25() : base(2020, 25)
        {
            input = File.ReadAllLines(FilePath).Select(long.Parse).ToArray();
        }

        public override string GetPartOne()
        {
            var size = GetLoopSize(input[1]);
            long current = 1;
            for (int i = 1; i <= size; i++)
            {
                current = current * input[0] % 20201227;
            }

            return current.ToString();
        }

        public override string GetPartTwo()
        {
            return "Congratz, it's over.";
        }

        private long GetLoopSize(long key)
        {
            long current = 1;
            int i = 1;
            while (true)
            {
                current = current * 7 % 20201227;
                if (key == current)
                {
                    return i;
                }
                i++;
            }
        }
    }
}
