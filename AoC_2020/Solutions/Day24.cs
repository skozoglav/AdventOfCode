﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2020.Solutions
{
    public class Day24 : BaseSolution
    {
        private readonly string[] input;
        private readonly Regex regex;
        private readonly Dictionary<string, (int x, int y, int z)> directions;
        private Dictionary<(int x, int y, int z), bool> tiles;



        public Day24() : base(2020, 24)
        {
            input = File.ReadAllLines(FilePath);
            regex = new Regex(@"^(?<move>e|se|sw|w|nw|ne)+", RegexOptions.Compiled);

            directions = new Dictionary<string, (int x, int y, int z)>() {
                { "e", (1, 0, -1) },
                { "w", (-1, 0, 1) },
                { "ne", (1, -1, 0) },
                { "nw", (0, -1, 1) },
                { "se", (0, 1, -1) },
                { "sw", (-1, 1, 0) },
            };
        }

        public override string GetPartOne()
        {
            var sequences = input.Select(l => regex.Matches(l).SelectMany(m => m.Groups["move"].Captures.Select(c => directions[c.Value])).ToArray()).ToArray();

            var locations = sequences.Select(q => q.Aggregate((x: 0, y: 0, z: 0), (s, d) => (s.x + d.x, s.y + d.y, s.z + d.z))).ToArray();
            tiles = locations.ToLookup(l => l).ToDictionary(g => g.Key, g => g.Count() % 2 == 1);

            return tiles.Count(tiles => tiles.Value).ToString();
        }

        public override string GetPartTwo()
        {
            var remove = tiles.Where(t => !t.Value).Select(t => t.Key);
            foreach (var r in remove) tiles.Remove(r);

            var neighbors = directions.Select(d => d.Value).ToArray();

            void tick()
            {
                var info = tiles.Select(t => (key: t.Key, value: t.Value, count:
                    neighbors.Select(n => (x: t.Key.x + n.x, y: t.Key.y + n.y, z: t.Key.z + n.z)).Count(a => tiles.ContainsKey(a) && tiles[a]))).ToArray();
                var to_white = info.Where(i => i.value && i.count == 0 || i.count > 2).ToArray();
                var to_black = info.Where(i => !i.value && i.count == 2).ToArray();
                var adjacent = tiles.SelectMany(t => neighbors.Select(n => (x: t.Key.x + n.x, y: t.Key.y + n.y, z: t.Key.z + n.z)))
                    .Distinct().Where(a => neighbors.Select(n => (x: a.x + n.x, y: a.y + n.y, z: a.z + n.z)).Count(a => tiles.ContainsKey(a) && tiles[a]) == 2).ToArray();
                foreach (var (key, _, _) in to_white) tiles.Remove(key);
                foreach (var (key, _, _) in to_black) tiles[key] = true;
                foreach (var a in adjacent) tiles[a] = true;
            }

            for (int i = 0; i < 100; ++i)
            {
                tick();
            }

            return tiles.Count(tiles => tiles.Value).ToString();
        }
    }
}
