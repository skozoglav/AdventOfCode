﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2020.Solutions
{
    public class Day06 : BaseSolution
    {
        private readonly string[] input;

        public Day06() : base(2020, 6)
        {
            input = Regex.Split(File.ReadAllText(FilePath), "\n\n");
        }

        public override string GetPartOne()
        {
            return input.Select(l => l.Replace("\n", "")
                    .Distinct().Count()).Sum().ToString();
        }

        public override string GetPartTwo()
        {
            return input.Sum(t => t.Split('\n')
                .Aggregate((s, s1) => string.Concat(s.Intersect(s1))).Length).ToString();
        }
    }
}
