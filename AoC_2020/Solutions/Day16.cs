﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day16 : BaseSolution
    {
        private readonly string[] input;
        private readonly long[] myTicket;
        private readonly List<List<int>> nearbyTickets;
        private readonly Dictionary<string, (int min, int max)[]> criteria;

        public Day16() : base(2020, 16)
        {
            criteria = new Dictionary<string, (int min, int max)[]>();
            input = File.ReadAllText(FilePath).Split("\n\n").ToArray();
            input[0].Split("\n").ToList().ForEach(ProcessCriteria);
            myTicket = input[1].Split("\n")[1].Split(",").Select(long.Parse).ToArray();
            nearbyTickets = input[2].Split("\n")[1..].Select(x => x.Split(","))
                .Select(x => x.Select(y => int.Parse(y)).ToList()).ToList();
        }

        public override string GetPartOne()
        {
            var result = 0;
            var criteriaFlat = criteria.SelectMany(x => x.Value);
            foreach (var ticket in nearbyTickets)
            {
                foreach (var value in ticket)
                {
                    if (!criteriaFlat.Any(x => value >= x.min && value <= x.max))
                        result += value;
                }

            }
            return result.ToString();
        }

        public override string GetPartTwo()
        {
            var usedCriteria = new HashSet<string>();
            var result = new List<int>();

            var allCriterion = criteria.SelectMany(x => x.Value).ToList();
            var validTickets = nearbyTickets.Where(x => IsValid(allCriterion, x)).ToList();
            var transposedTickets = Transpose(validTickets);

            var matchingFields = transposedTickets.Select((a, i) => (Fields: criteria.Keys.Where(x => AllNumbersMatch(x, a)), Index: i)).OrderBy(x => x.Fields.Count()).ToList();

            foreach (var _ in transposedTickets)
            {
                var (Fields, Index) = matchingFields.First();

                if (Fields.First().Contains("depart"))
                {
                    result.Add(Index);
                }

                usedCriteria.Add(Fields.First());
                matchingFields = matchingFields.Select(x => (Fields: x.Fields.Where(y => !usedCriteria.Contains(y)), x.Index)).Where(x => x.Fields.Count() > 0).OrderBy(x => x.Fields.Count()).ToList();
            }

            return result.Select(x => myTicket[x]).Aggregate((x, y) => x * y).ToString();
        }

        #region Helper methods

        private void ProcessCriteria(string line)
        {
            var delimited = line.Split(" ");
            var key = string.Join(" ", delimited[..^3]);
            var value = new (int, int)[] { ProcessLimits(delimited[^1]), ProcessLimits(delimited[^3]) };
            criteria.Add(key, value);
        }
        private (int Min, int Max) ProcessLimits(string input)
        {
            var delimited = input.Split("-");
            return (int.Parse(delimited[0]), int.Parse(delimited[1]));
        }

        private bool IsValid(List<(int Min, int Max)> allCriterion, List<int> ticket)
        {
            return ticket.All(x => allCriterion.ToList().Any(y => x >= y.Min && x <= y.Max));
        }

        private bool AllNumbersMatch(string key, List<int> tickets) => tickets.All(x => criteria[key].Any(y => x >= y.min && x <= y.max));

        private List<List<int>> Transpose(List<List<int>> input)
        {
            var result = new List<List<int>>();
            for (int i = 0; i < input[0].Count(); i++)
            {
                result.Add(input.Select(x => x[i]).ToList());
            }
            return result;
        }

        #endregion

    }
}
