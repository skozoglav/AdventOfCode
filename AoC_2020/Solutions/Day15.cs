﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day15 : BaseSolution
    {
        private readonly int[] input;

        public Day15() : base(2020, 15)
        {
            input = File.ReadAllText(FilePath).Split(',').Select(int.Parse).ToArray();
        }

        public override string GetPartOne() => Calculate(input, 2020);

        public override string GetPartTwo() => Calculate(input, 30000000);

        private string Calculate(int[] numbers, int count)
        {
            var memory = numbers
                .Select((character, index) => (character, index))
                .ToDictionary(x => x.character, x => (-1, x.index));

            var lastNumber = memory.Last().Key;

            for (var i = numbers.Length; i < count; i++)
            {
                if (memory.TryGetValue(lastNumber, out var lastIndex))
                {
                    if (lastIndex.Item1 == -1)
                        lastNumber = 0;
                    else
                        lastNumber = lastIndex.index - lastIndex.Item1;
                }

                if (memory.ContainsKey(lastNumber))
                    memory[lastNumber] = (memory[lastNumber].index, i);
                else
                    memory[lastNumber] = (-1, i);
            }

            return lastNumber.ToString();
        }
    }
}
