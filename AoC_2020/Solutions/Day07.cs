﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2020.Solutions
{
    public class Day07 : BaseSolution
    {
        private const string key = "shiny gold";
        private readonly string[] input;
        private Dictionary<string, Dictionary<string, int>> dict;

        public Day07() : base(2020, 7)
        {
            input = File.ReadAllLines(FilePath);
            ParseInput(input);
        }

        public override string GetPartOne() =>
            dict.Keys.Count(CanContain).ToString();

        public override string GetPartTwo() =>
            MustContain(key).ToString();

        #region Helper methods

        /// <summary>
        /// Parses our input to a Dict<k, v> of colors. 
        /// k: color of the bag
        /// v: dict of colors in that bag
        /// 
        /// E.g. Red bags contain 2 Blue bags => <Red, <Blue, 2>>
        /// </summary>
        private void ParseInput(string[] input)
        {
            var regex = new Regex(@"(.*?) bags contain(?: (\d+ .*?) bag(?:s)?[,.])*");
            dict = input.Select(line => regex.Match(line))
                                .ToDictionary(
                                    m => m.Groups[1].Value,
                                    m => m.Groups[2].Captures.OfType<Capture>()
                                        .Select(c => c.Value.Split(new[] { ' ' }, 2))
                                        .ToDictionary(a => a[1], a => int.Parse(a[0])));
        }

        private bool CanContain(string color) => dict[color].ContainsKey(key) || dict[color].Keys.Any(CanContain);

        private int MustContain(string color) => dict[color].Sum(k => k.Value * (1 + MustContain(k.Key)));


        #endregion

    }
}
