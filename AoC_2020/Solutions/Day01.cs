﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day01 : BaseSolution
    {
        private readonly int[] input;

        public Day01() : base(2020, 1)
        {
            input = Array.ConvertAll(System.IO.File.ReadAllLines(FilePath), s => int.Parse(s));
        }

        public override string GetPartOne()
        {
            int size = input.Length;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if ((input[i] + input[j]) == 2020)
                    {
                        return $"{input[i] * input[j]}";
                    }
                }
            }

            return String.Empty;
        }

        public override string GetPartTwo()
        {
            int size = input.Length;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    for (int k = 0; k < size; k++)
                    {
                        if ((input[i] + input[j] + input[k]) == 2020)
                        {
                            return $"{input[i] * input[j] * input[k]}";
                        }
                    }
                }
            }
            return String.Empty;
        }
    }
}
