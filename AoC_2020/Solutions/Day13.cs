﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day13 : BaseSolution
    {
        private readonly int timeEstimate;
        private readonly (long, int)[] busIDs;

        public Day13() : base(2020, 13)
        {
            var input = File.ReadAllLines(FilePath);
            timeEstimate = int.Parse(input[0]);
            busIDs = input[1].Split(',')
                .Select((item, index) => (item, index))
                .Where(x => x.item != "x")
                .Select(x => (long.Parse(x.item), x.index)).ToArray();
        }

        public override string GetPartOne()
        {
            var time = timeEstimate;
            while (true)
            {
                foreach ((var bus, _) in busIDs)
                {
                    if (time % bus == 0)
                    {
                        return ((time - timeEstimate) * bus).ToString();
                    }
                }
                time += 1;
            }
        }

        public override string GetPartTwo()
        {
            var timestamp = busIDs[0].Item1 - busIDs[0].Item2;
            var period = busIDs[0].Item1;

            for (var index = 1; index <= busIDs.Length; index++)
            {
                while (busIDs.Take(index).Any(t => (timestamp + t.Item2) % t.Item1 != 0))
                {
                    timestamp += period;
                }

                period = busIDs.Take(index).Select(t => t.Item1).Aggregate(LCM);
            }

            return timestamp.ToString();
        }


        private long LCM(long a, long b)
        {
            long num1, num2;
            num1 = a > b ? a : b;
            num2 = a > b ? b : a;

            for (int i = 1; i < num2; i++)
            {
                long mult = num1 * i;
                if (mult % num2 == 0)
                {
                    return mult;
                }
            }
            return num1 * num2;
        }
    }
}
