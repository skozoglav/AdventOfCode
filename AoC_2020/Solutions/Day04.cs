﻿using AoC.Common;
using System.Text.RegularExpressions;
using Passports = System.Collections.Generic.IEnumerable<System.Collections.Generic.IEnumerable<string>>;
using Values = System.Collections.Generic.IEnumerable<string>;

namespace AoC_2020.Solutions
{
    public class Day04 : BaseSolution
    {
        private readonly Passports passports;

        public Day04() : base(2020, 4)
        {
            passports = Regex.Split(File.ReadAllText(FilePath), "\n\n")
                .Select(x => x.Replace("\n", " "))
                .Select(ParseValues);
        }

        public override string GetPartOne() =>
            passports.Where(ValidFormat).Count().ToString();

        public override string GetPartTwo() =>
            passports.Where(ValidFormat)
                .Where(ValidValues)
                .Count().ToString();


        #region Helper methods

        private static bool ValidFormat(Values values) =>
            new string[] { "byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:" }
                .All(start => values.Any(x => x.StartsWith(start)));

        private static bool ValidValue(string value) =>
            new Regex(value.Substring(0, 3) switch
            {
                "byr" => "^byr:(19[2-9][0-9]|200[0-2])$",
                "iyr" => "^iyr:(201[0-9]|2020)$",
                "eyr" => "^eyr:(202[0-9]|2030)$",
                "hgt" => "^hgt:(1([5-8][0-9]|9[0-3])cm|(59|6[0-9]|7[0-6])in)$",
                "hcl" => "^hcl:#[a-f0-9]{6}$",
                "ecl" => "^ecl:(amb|blu|brn|gry|grn|hzl|oth)$",
                "pid" => "^pid:[0-9]{9}$",
                "cid" => "^cid:",
                _ => "$^",
            }).IsMatch(value);

        private static bool ValidValues(Values values) =>
            values.All(ValidValue);

        private static Values ParseValues(string input) =>
            input.Split(' ');

        #endregion
    }
}
