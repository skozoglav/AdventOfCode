﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2020.Solutions
{
    public class Day14 : BaseSolution
    {
        private readonly string[] input;
        private readonly Dictionary<string, long> addresses;

        private const string _maskRegex = @"mask = (?<mask>[X01]+)";
        private const string _memoryRegex = @"mem\[(?<address>\d+)\] = (?<value>(\d+))";

        public Day14() : base(2020, 14)
        {
            input = File.ReadAllLines(FilePath);
            addresses = new Dictionary<string, long>();
        }

        public override string GetPartOne()
        {
            return Calculate(Task.PartOne);
        }

        public override string GetPartTwo()
        {
            addresses.Clear();
            return Calculate(Task.PartTwo);
        }

        private string Calculate(Task task)
        {
            string bitmask = input[0].Split(" ")[^1];

            foreach (var line in input.Skip(1))
            {
                if (Regex.IsMatch(line, _maskRegex))
                {
                    bitmask = line.Split(" ")[^1];
                    continue;
                }

                var matches = Regex.Match(line, _memoryRegex).Groups;

                var value = task.Equals(Task.PartTwo)
                    ? Convert.ToString(long.Parse(matches["address"].Value), 2).PadLeft(bitmask.Length, '0')
                    : Convert.ToString(long.Parse(matches["value"].Value), 2).PadLeft(bitmask.Length, '0');

                var maskAndValues = bitmask.Zip(value, (x, y) => (Mask: x, Value: y));

                var result = task.Equals(Task.PartTwo)
                    ? string.Concat(maskAndValues.Select(x => x.Mask != '0' ? x.Mask : x.Value))
                    : string.Concat(maskAndValues.Select(x => x.Mask != 'X' ? x.Mask : x.Value));

                var addrs = task.Equals(Task.PartTwo)
                    ? GenerateCombinations(result).Select(x => Convert.ToInt64(x, 2).ToString()).ToList()
                    : new List<string> { matches["address"].Value };

                foreach (var address in addrs)
                {
                    addresses[address] = task.Equals(Task.PartTwo)
                        ? long.Parse(matches["value"].Value)
                        : Convert.ToInt64(result, 2);
                }
            }

            return addresses.Keys.Sum(x => addresses[x]).ToString();
        }

        private static List<string> GenerateCombinations(string value)
        {
            if (!value.Any(c => c.Equals('X')))
            {
                return new List<string> { value };
            }
            else
            {
                var zeroMask = ReplaceFirstMatch(value, "X", "0");
                var oneMask = ReplaceFirstMatch(value, "X", "1");
                return GenerateCombinations(zeroMask).Concat(GenerateCombinations(oneMask)).ToList();
            }
        }

        private static string ReplaceFirstMatch(string value, string mask, string replacement)
        {
            var firstMaskIndex = value.IndexOf(mask);
            if (firstMaskIndex < 0)
            {
                return value;
            }
            return value.Remove(firstMaskIndex, 1).Insert(firstMaskIndex, replacement);
        }

        private enum Task
        {
            PartOne,
            PartTwo
        }
    }
}
