﻿using AoC.Common;


namespace AoC_2020.Solutions
{
    public class Day03 : BaseSolution
    {
        private readonly string[,] trees;

        public Day03() : base(2020, 3)
        {
            string[] input = File.ReadAllLines(FilePath);
            trees = ConvertTo2dArray(input);
        }

        public override string GetPartOne()
        {
            return FindTrees(trees, 3, 1).ToString();
        }

        public override string GetPartTwo()
        {
            Int64 solution = FindTrees(trees, 1, 1);
            solution *= FindTrees(trees, 3, 1);
            solution *= FindTrees(trees, 5, 1);
            solution *= FindTrees(trees, 7, 1);
            solution *= FindTrees(trees, 1, 2);

            return solution.ToString();
        }

        #region Helper methods

        private string[,] ConvertTo2dArray(string[] input)
        {
            string[,] trees = new string[input.Length, input[0].Length];

            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input[i].Length; j++)
                {
                    trees[i, j] = input[i][j].ToString();
                }
            }

            return trees;
        }

        private int FindTrees(string[,] trees, int moveX, int moveY)
        {
            int x = 0, y = 0, conter = 0;
            while (y < trees.GetLength(0))
            {
                if (trees[y, x] == "#")
                {
                    conter++;
                }
                y += moveY;
                x = (x + moveX) % trees.GetLength(1);
            }
            return conter;
        }

        #endregion
    }
}
