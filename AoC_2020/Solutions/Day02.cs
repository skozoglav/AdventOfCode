﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day02 : BaseSolution
    {
        private readonly List<Password> passwords;

        public Day02() : base(2020, 2)
        {
            string[] input = File.ReadAllLines(FilePath);
            passwords = ParseInput(input);
        }

        public override string GetPartOne()
        {
            int counter = 0;

            foreach (var password in passwords)
            {
                int reppetitions = password.Passwd.Count(x => x == password.Letter);
                if (reppetitions >= password.Min && reppetitions <= password.Max)
                {
                    counter++;
                }
            }

            return counter.ToString();
        }

        public override string GetPartTwo()
        {
            int counter = 0;

            foreach (var password in passwords)
            {
                if ((password.Passwd[password.Min - 1] == password.Letter &&
                     password.Passwd[password.Max - 1] != password.Letter) ||
                    (password.Passwd[password.Min - 1] != password.Letter &&
                     password.Passwd[password.Max - 1] == password.Letter))
                {
                    counter++;
                }
            }

            return counter.ToString();
        }

        #region Helper stuff

        private List<Password> ParseInput(string[] input)
        {
            var passwords = new List<Password>();
            foreach (var l in input)
            {
                string[] line = l.Split(' ');
                string[] first = line[0].Split('-');

                passwords.Add(new Password()
                {
                    Min = int.Parse(first[0]),
                    Max = int.Parse(first[1]),
                    Letter = char.Parse(line[1].Substring(0, 1)),
                    Passwd = line[2]
                });
            }

            return passwords;
        }

        private class Password
        {
            public int Min { get; set; }
            public int Max { get; set; }
            public char Letter { get; set; }
            public string Passwd { get; set; }
        }

        #endregion
    }
}
