﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day05 : BaseSolution
    {
        private readonly string[] input;
        private readonly List<int> filledSeats;

        public Day05() : base(2020, 5)
        {
            input = File.ReadAllLines(FilePath);
            filledSeats = new List<int>();
        }

        public override string GetPartOne()
        {
            int highestSeat = 0;
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = input[i].Replace('F', '0');
                input[i] = input[i].Replace('L', '0');
                input[i] = input[i].Replace('B', '1');
                input[i] = input[i].Replace('R', '1');

                filledSeats.Add(Convert.ToInt32(input[i], 2));  // for PartTwo
                highestSeat = Math.Max(highestSeat, Convert.ToInt32(input[i], 2));
            }

            return highestSeat.ToString();
        }

        public override string GetPartTwo()
        {
            for (int seat = 8; seat < 1016; seat++)
            {
                if (filledSeats.Contains(seat - 1) && filledSeats.Contains(seat + 1) && !filledSeats.Contains(seat))
                {
                    return seat.ToString();
                }
            }

            return String.Empty;
        }
    }
}
