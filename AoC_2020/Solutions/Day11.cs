﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day11 : BaseSolution
    {
        private readonly int[][] input;
        private readonly (int, int)[] directions = { (-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1) };

        public Day11() : base(2020, 11)
        {
            input = File.ReadAllLines(FilePath)
                .Select(x => x.ToCharArray().Select(y => y == '.' ? -1 : 0).ToArray()).ToArray();
        }

        public override string GetPartOne() => CalculateState(4, 1).ToString();

        public override string GetPartTwo() => CalculateState(5, 100).ToString();

        #region Helper methods

        private int CalculateState(int tolerance, int range)
        {
            var grid = input.Clone() as int[][];
            var next = Iterate(grid, tolerance, range);

            while (!AreSame(grid, next))
            {
                var tmp = next;
                next = Iterate(next, tolerance, range);
                grid = tmp;
            }

            return grid.SelectMany(x => x).Where(y => y == 1).Count();
        }

        private bool AreSame(int[][] g0, int[][] g1)
        {
            for (int r = 0; r < g0.Length; r++)
            {
                for (int c = 0; c < g0[0].Length; c++)
                {
                    if (g0[r][c] != g1[r][c])
                        return false;
                }
            }

            return true;
        }

        private int[][] Iterate(int[][] grid, int tolerance, int range)
        {
            int rowLength = grid.Length;
            int colLength = grid[0].Length;

            int[][] next = new int[rowLength][];

            for (int r = 0; r < rowLength; r++)
            {
                int[] row = new int[colLength];
                for (int c = 0; c < colLength; c++)
                {
                    if (grid[r][c] == -1)
                    {
                        row[c] = -1;
                    }

                    int occupied = CountOccupied(grid, r, c, range);
                    if (grid[r][c] == 0 && occupied == 0)
                    {
                        row[c] = 1;
                    }
                    else if (grid[r][c] == 1 && occupied >= tolerance)
                    {
                        row[c] = 0;
                    }
                    else
                    {
                        row[c] = grid[r][c];
                    }
                }
                next[r] = row;
            }

            return next;
        }

        private int CountOccupied(int[][] grid, int row, int col, int range)
        {
            int occupied = 0;
            foreach (var d in directions)
            {
                occupied += RayCast(grid, row, col, d, range);
            }

            return occupied;
        }

        private int RayCast(int[][] grid, int row, int col, (int, int) dir, int range)
        {
            for (int j = 0; j < range; j++)
            {
                row += dir.Item1;
                col += dir.Item2;

                if (row < 0 || col < 0 || row >= grid.Length || col >= grid[0].Length)
                    return 0;

                if (grid[row][col] != -1)
                    return grid[row][col];
            }

            return 0;
        }

        #endregion
    }
}
