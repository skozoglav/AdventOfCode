﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day23 : BaseSolution
    {
        private readonly string input;

        public Day23() : base(2020, 23)
        {
            input = File.ReadAllText(FilePath);
        }

        public override string GetPartOne() => PlayCrabGame(input, 0, 100, false);

        public override string GetPartTwo() => PlayCrabGame(input, 1000000, 10000000, true);


        private int FindDestination(int start, Node cut, int highestID)
        {
            int dest = start == 1 ? highestID : start - 1;
            int a = cut.Val;
            int b = cut.Next.Val;
            int c = cut.Next.Next.Val;

            while (dest == a || dest == b || dest == c)
            {
                --dest;
                if (dest <= 0)
                    dest = highestID;
            }

            return dest;
        }

        private string ListToString(Node node, int startVal)
        {
            while (node.Val != startVal)
                node = node.Next;
            node = node.Next;

            var str = "";
            do
            {
                str += node.Val.ToString();
                node = node.Next;
            } while (node.Val != startVal);

            return str;
        }

        private string PlayCrabGame(string initial, int max, int rounds, bool isPartTwo)
        {
            var nums = initial.ToCharArray().Select(n => (int)n - (int)'0').ToList();
            int highestID = nums.Max();

            if (max > 0)
                nums.AddRange(Enumerable.Range(highestID + 1, max - nums.Count));

            Node[] index = new Node[max == 0 ? 10 : max + 1];
            Node start = new Node(nums.First());
            index[nums.First()] = start;
            Node prev = start;
            foreach (int v in nums.Skip(1))
            {
                Node n = new Node(v);
                index[v] = n;
                prev.Next = n;
                prev = n;

                if (v > highestID)
                    highestID = v;
            }
            prev.Next = start;

            Node curr = start;
            for (int j = 0; j < rounds; j++)
            {
                Node cut = curr.Next;
                curr.Next = cut.Next.Next.Next;

                int destVal = FindDestination(curr.Val, cut, highestID);

                Node ip = index[destVal];
                Node ipn = ip.Next;
                Node tail = cut.Next.Next;
                tail.Next = ipn;
                ip.Next = cut;

                curr = curr.Next;
            }

            if (!isPartTwo)
                return ListToString(start, 1);
            else
            {
                Node n = index[1];
                ulong res = (ulong)n.Next.Val * (ulong)n.Next.Next.Val;
                return res.ToString();
            }
        }

        private class Node
        {
            public int Val { get; set; }
            public Node Next { get; set; }

            public Node(int v)
            {
                Val = v;
            }
        }
    }
}
