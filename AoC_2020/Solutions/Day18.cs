﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2020.Solutions
{
    public class Day18 : BaseSolution
    {
        private readonly string[] input;
        private readonly Regex bracketMatcher = new Regex(@"\(([^()]+)\)");

        public Day18() : base(2020, 18)
        {
            input = File.ReadAllLines(FilePath);
        }

        public override string GetPartOne()
        {
            long p1 = 0;
            foreach (var line in input)
            {
                var l = line;
                while (bracketMatcher.IsMatch(l))
                {
                    l = bracketMatcher.Replace(l, m => EvaluateP1(m.Groups[1].Value));
                }
                p1 += long.Parse(EvaluateP1(l));
            }

            return p1.ToString();
        }

        public override string GetPartTwo()
        {
            long p2 = 0;
            foreach (var line in input)
            {
                var l = line;
                while (bracketMatcher.IsMatch(l))
                {
                    l = bracketMatcher.Replace(l, m => EvaluateP2(m.Groups[1].Value));
                }
                p2 += long.Parse(EvaluateP2(l));
            }
            return p2.ToString();
        }


        string EvaluateP1(string input)
        {
            var opper = new Regex(@"(\d+) (\+|\*) (\d+)");
            while (opper.IsMatch(input))
            {
                input = opper.Replace(
                    input,
                    m => m.Groups[2].Value == "+"
                        ? (long.Parse(m.Groups[1].Value) + long.Parse(m.Groups[3].Value)).ToString()
                        : (long.Parse(m.Groups[1].Value) * long.Parse(m.Groups[3].Value)).ToString(),
                    1);
            }

            return input;
        }

        string EvaluateP2(string input)
        {
            var plusser = new Regex(@"(\d+) \+ (\d+)");
            while (plusser.IsMatch(input))
            {
                input = plusser.Replace(input, m => (long.Parse(m.Groups[1].Value) + long.Parse(m.Groups[2].Value)).ToString(), 1);
            }

            var multer = new Regex(@"(\d+) \* (\d+)");
            while (multer.IsMatch(input))
            {
                input = multer.Replace(input, m => (long.Parse(m.Groups[1].Value) * long.Parse(m.Groups[2].Value)).ToString(), 1);
            }

            return input;
        }
    }
}
