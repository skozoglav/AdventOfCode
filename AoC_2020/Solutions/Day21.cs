﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day21 : BaseSolution
    {
        private readonly string[] input;

        public Day21() : base(2020, 21)
        {
            input = File.ReadAllLines(FilePath);
        }

        public override string GetPartOne()
        {
            var items = new List<Item>();
            foreach (var line in input)
            {
                var it = new Item();
                var sp = line.Split(" (");
                var prev = sp[0].Split(' ').Select(a => a.Trim()).ToList();
                it.Ingredients = prev;
                var n = sp[1][0..^1].Replace("contains", "").Split(',').Select(a => a.Trim()).ToList();
                it.Allergens = n;
                items.Add(it);
            }
            var changed = true;
            while (changed)
            {
                var oCount = Item.AllergensDb.Count;
                var unkAll = items.SelectMany(a => a.GetUnknownAllergens()).Distinct();
                foreach (var all in unkAll)
                {
                    var lst = items.Where(a => a.GetUnknownAllergens().Contains(all)).Select(a => a.GetUnknownIngredients()).ToList();
                    var intersec = lst.Aggregate((a, b) => a.Intersect(b));
                    if (intersec.Count() == 1)
                    {
                        Item.AllergensDb[all] = intersec.Single();
                    }
                }
                changed = Item.AllergensDb.Count != oCount;
            }
            var notIngredients = items.SelectMany(a => a.Ingredients).Except(Item.AllergensDb.Values).ToList();
            var count = 0;
            foreach (var itm in items)
            {
                foreach (var ig in notIngredients)
                    if (itm.Ingredients.Contains(ig)) count++;
            }
            return count.ToString();
        }

        public override string GetPartTwo() => Item.AllergensDb.OrderBy(a => a.Key).Select(a => a.Value).Aggregate((a, b) => a + "," + b);

        private class Item
        {
            public static Dictionary<string, string> AllergensDb { get; set; } = new Dictionary<string, string>();
            public List<string> Ingredients { get; set; } = new List<string>();
            public List<string> Allergens { get; set; } = new List<string>();
            public IEnumerable<string> GetUnknownIngredients()
            {
                foreach (var ing in Ingredients)
                    if (!AllergensDb.Values.Contains(ing))
                        yield return ing;
            }
            public IEnumerable<string> GetUnknownAllergens()
            {
                foreach (var all in Allergens)
                    if (!AllergensDb.ContainsKey(all))
                        yield return all;
            }
        }
    }
}
