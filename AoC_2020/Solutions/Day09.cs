﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day09 : BaseSolution
    {
        private readonly long[] input;
        private readonly long target;

        public Day09() : base(2020, 9)
        {
            input = File.ReadAllLines(FilePath).Select(long.Parse).ToArray();
            target = long.Parse(GetPartOne());
        }

        public override string GetPartOne()
        {
            var preamble = input.Take(25).ToList();
            var others = input.Skip(25).ToList();

            foreach (var i in others)
            {
                if (!IsSumOf(preamble, i))
                {
                    return i.ToString();
                }
                preamble.RemoveAt(0);
                preamble.Add(i);
            }

            return string.Empty;
        }

        public override string GetPartTwo()
        {
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = i + 2; j < input.Length; j++)
                {
                    long[] temp = new long[j - i];
                    Array.Copy(input, i, temp, 0, j - i);

                    long sum = temp.Sum();
                    if (sum == target)
                    {
                        return (temp.Min() + temp.Max()).ToString();
                    }

                    if (sum > target) { break; }
                }
            }

            return string.Empty;
        }

        #region Helper methods

        private bool IsSumOf(List<long> preamble, long x)
        {
            foreach (var i in preamble)
            {
                if (preamble.Contains(x - i))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

    }
}
