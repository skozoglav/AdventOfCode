﻿using AoC.Common;

namespace AoC_2020.Solutions
{
    public class Day08 : BaseSolution
    {
        private string[][] code;

        public Day08() : base(2020, 8)
        {
            code = File.ReadAllLines(FilePath)
                .Select(x => x.Split(' '))
                .Select(y => new string[] { y[0], y[1] }).ToArray();
        }

        public override string GetPartOne()
        {
            var (_, acc) = RunBootCode(code);
            return acc.ToString();
        }

        public override string GetPartTwo()
        {
            int acc, posChange = 0;
            bool finished;

            while (posChange < code.GetLength(0))
            {
                code = Flip(code, posChange);
                (finished, acc) = RunBootCode(code);

                if (finished)
                {
                    return acc.ToString();
                }

                code = Flip(code, posChange);
                posChange += 1;
            }

            return string.Empty;
        }

        #region Helper methods

        private (bool, int) RunBootCode(string[][] code)
        {
            int acc = 0, position = 0;
            var visitedInstruction = new List<int>();

            while (!visitedInstruction.Contains(position))
            {
                visitedInstruction.Add(position);
                if (code[position][0] == "acc")
                {
                    acc += int.Parse(code[position][1]);
                    position += 1;
                }
                else if (code[position][0] == "jmp")
                {
                    position += int.Parse(code[position][1]);
                }
                else
                {
                    position += 1;
                }

                if (position == code.GetLength(0) - 1)
                {
                    return (true, acc);
                }
            }

            return (false, acc);
        }

        private string[][] Flip(string[][] code, int pos)
        {
            if (code[pos][0] == "jmp")
            {
                code[pos][0] = "nop";
            }
            else if (code[pos][0] == "nop")
            {
                code[pos][0] = "jmp";
            }

            return code;
        }

        #endregion
    }
}
