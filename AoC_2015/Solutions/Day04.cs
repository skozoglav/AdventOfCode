﻿using AoC.Common;
using System.Security.Cryptography;
using System.Text;

namespace AoC_2015.Solutions
{
    public class Day04 : BaseSolution
    {
        private readonly string input;
        private static readonly MD5 md5 = MD5.Create();

        public Day04() : base(2015, 4) => input = File.ReadAllText(FilePath);

        public override string GetPartOne() => CheckHash();

        public override string GetPartTwo()
        {
            var number = CheckHash(true);
            md5.Dispose();
            return number;
        }

        private string CheckHash(bool isPartTwo = false)
        {
            int i = 0;
            while (i < int.MaxValue)
            {
                var hash = CreateMD5(input + i++);
                if (hash[0] == 0 && hash[1] == 0)
                {
                    // hash[2] from 0x00 to 0x0f
                    if ((!isPartTwo && hash[2] < 16) ||
                        (isPartTwo && hash[2] == 0))
                    {
                        return (i - 1).ToString();
                    }
                }
            }
            return "0";
        }

        private static byte[] CreateMD5(string input)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            return md5.ComputeHash(inputBytes);
        }
    }
}
