﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day06 : BaseSolution
    {
        private readonly Instruction[] input;
        private readonly (bool state, int brightness)[,] grid;

        public Day06() : base(2015, 6)
        {
            input = ParseInput();
            grid = new (bool, int)[1000, 1000];
        }

        public override string GetPartOne()
        {
            Iterate();
            return CountLights();
        }

        public override string GetPartTwo()
        {
            //grid = new (bool, int)[1000, 1000];
            //Iterate();

            return CountBrightness();
        }

        private void Iterate()
        {
            foreach (var instruction in input)
            {
                switch (instruction.InstructionState)
                {
                    case State.TurnOn:
                        SetGrid(instruction, true);
                        break;
                    case State.Toggle:
                        ToogleGrid(instruction);
                        break;
                    case State.TurnOff:
                        SetGrid(instruction, false);
                        break;
                    default: return;
                }
            }
        }

        private Instruction[] ParseInput()
        {
            var lines = InputParser.FileToStringArray(FilePath);
            var instructions = new Instruction[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] items;

                State state;
                if (lines[i].StartsWith("toggle"))
                {
                    state = State.Toggle;
                    items = lines[i][7..].Split(' ');
                }
                else
                {
                    items = lines[i].Split(' ');
                    state = items[1] == "on" ? State.TurnOn : State.TurnOff;
                    items = items[2..];
                }

                instructions[i] = new Instruction
                {
                    InstructionState = state,
                    X0 = int.Parse(items[0].Split(',')[0]),
                    Y0 = int.Parse(items[0].Split(',')[1]),
                    X1 = int.Parse(items[2].Split(',')[0]),
                    Y1 = int.Parse(items[2].Split(',')[1])
                };
            }

            return instructions;
        }

        private void SetGrid(Instruction instruction, bool state)
        {
            for (int i = instruction.X0; i <= instruction.X1; i++)
            {
                for (int j = instruction.Y0; j <= instruction.Y1; j++)
                {
                    grid[i, j] = (state, state ? grid[i, j].brightness + 1 : grid[i, j].brightness > 0 ? grid[i, j].brightness - 1 : 0);
                }
            }
        }

        private void ToogleGrid(Instruction instruction)
        {
            for (int i = instruction.X0; i <= instruction.X1; i++)
            {
                for (int j = instruction.Y0; j <= instruction.Y1; j++)
                {
                    grid[i, j] = (!grid[i, j].state, grid[i, j].brightness + 2);
                }
            }
        }

        private string CountLights()
        {
            int count = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[i, j].state)
                    {
                        count++;
                    }
                }
            }
            return count.ToString();
        }

        private string CountBrightness()
        {
            int count = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    count += grid[i, j].brightness;
                }
            }
            return count.ToString();
        }
    }

    enum State
    {
        TurnOn,
        Toggle,
        TurnOff
    }

    struct Instruction
    {
        public State InstructionState;
        public int X0;
        public int Y0;
        public int X1;
        public int Y1;
    }

}
