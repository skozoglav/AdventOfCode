﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day03 : BaseSolution
    {
        private readonly char[] input;
        private readonly Dictionary<(int, int), int> santaGrid;

        public Day03() : base(2015, 3)
        {
            input = InputParser.FileToCharArray(FilePath);
            santaGrid = new Dictionary<(int, int), int>();
        }

        public override string GetPartOne()
        {
            int x = 0, y = 0;
            AddToGrid(x, y);

            foreach (var move in input)
            {
                (x, y) = GetPossition(x, y, move);
                AddToGrid(x, y);
            }

            return santaGrid.Count().ToString();
        }

        public override string GetPartTwo()
        {
            santaGrid.Clear();

            int x = 0, y = 0, xr = 0, yr = 0;
            AddToGrid(x, y);

            for (int i = 0; i < input.Length; i++)
            {
                if (i % 2 == 0)
                {
                    (x, y) = GetPossition(x, y, input[i]);
                    AddToGrid(x, y);
                }
                else
                {
                    (xr, yr) = GetPossition(xr, yr, input[i]);
                    AddToGrid(xr, yr);
                }
            }

            return santaGrid.Count().ToString();
        }

        private void AddToGrid(int x, int y)
        {
            if (!santaGrid.ContainsKey((x, y)))
            {
                santaGrid.Add((x, y), 1);
            }
            else
            {
                santaGrid[(x, y)] += 1;
            }
        }

        private (int, int) GetPossition(int x, int y, char move)
        {
            return move switch
            {
                '^' => (x, y + 1),
                'v' => (x, y - 1),
                '>' => (x + 1, y),
                '<' => (x - 1, y),
                _ => throw new ArgumentException("Invalid move."),
            };
        }
    }
}
