﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day07 : BaseSolution
    {
        private readonly string[] input;
        private readonly Dictionary<string, int> circuit;
        private readonly List<Operator> operators;

        public Day07() : base(2015, 7)
        {
            input = InputParser.FileToStringArray(FilePath);
            circuit = new Dictionary<string, int>();
            operators = new List<Operator>();
        }

        public override string GetPartOne()
        {
            Iterate();
            foreach (Operator op in operators)
            {
                GetValues(op);
            }

            return circuit["a"].ToString();
        }

        public override string GetPartTwo()
        {
            //circuit.Clear();
            //operators.Clear();
            //Iterate();

            var op = operators.FirstOrDefault(o => o.result == "b");
            operators.Remove(op);
            op.leftOp = circuit["a"].ToString();
            operators.Add(op);
            circuit.Clear();
            foreach (var opr in operators)
            {
                GetValues(opr);
            }

            return circuit["a"].ToString();
        }

        private void Iterate()
        {
            foreach (var line in input)
            {
                var data = line.Split(' ');
                Operator op = new Operator();
                switch (data.Length)
                {
                    case 3:
                        op.Op = "Pass";
                        op.leftOp = data[0];
                        op.result = data[2];
                        break;
                    case 4:
                        op.Op = data[0];
                        op.rightOp = data[1];
                        op.result = data[3];
                        break;
                    case 5:
                        op.Op = data[1];
                        op.leftOp = data[0];
                        op.rightOp = data[2];
                        op.result = data[4];
                        break;
                }
                operators.Add(op);
            }
        }

        private int GetValue(string operand)
        {
            if (Int32.TryParse(operand, out int val)) return val;
            if (!circuit.ContainsKey(operand))
            {
                Operator op = operators.FirstOrDefault(o => o.result == operand);
                GetValues(op);
            }
            val = circuit[operand];
            return val;
        }

        private void GetValues(Operator op)
        {
            switch (op.Op)
            {
                case "Pass":
                    circuit[op.result] = GetValue(op.leftOp);
                    break;
                case "NOT":
                    circuit[op.result] = ~GetValue(op.rightOp);
                    break;
                case "OR":
                    circuit[op.result] = GetValue(op.leftOp) | GetValue(op.rightOp);
                    break;
                case "AND":
                    circuit[op.result] = GetValue(op.leftOp) & GetValue(op.rightOp);
                    break;
                case "RSHIFT":
                    circuit[op.result] = GetValue(op.leftOp) >> GetValue(op.rightOp);
                    break;
                case "LSHIFT":
                    circuit[op.result] = GetValue(op.leftOp) << GetValue(op.rightOp);
                    break;
            }
        }

    }

    struct Operator
    {
        public string leftOp;
        public string rightOp;
        public string Op;
        public string result;
    }

}
