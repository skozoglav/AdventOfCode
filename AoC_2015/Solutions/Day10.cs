﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2015.Solutions
{
    public class Day10 : BaseSolution
    {
        private readonly string input;

        public Day10() : base(2015, 10)
        {
            this.input = File.ReadAllText(FilePath);
        }

        public override string GetPartOne() => ExecutePart(40);

        public override string GetPartTwo() => ExecutePart(50);

        private string ExecutePart(int iterations)
        {
            string result = this.input;
            for (int i = 0; i < iterations; i++)
            {
                result = LookAndSay(result);
            }

            return result.Length.ToString();
        }

        private static string LookAndSay(string input)
        {
            var captures = Regex.Match(input, @"((.)\2*)+").Groups[1].Captures;
            return string.Concat(
                from c in captures.Cast<Capture>()
                let v = c.Value
                select v.Length + v.Substring(0, 1));
        }
    }
}
