﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2015.Solutions
{
    public class Day05 : BaseSolution
    {
        private readonly string[] input;

        public Day05() : base(2015, 5) => input = InputParser.FileToStringArray(FilePath);

        public override string GetPartOne()
        {
            int niceStrings = 0;
            var vowels = new Regex(@"([aeiou].*){3,}");
            var doubleLetters = new Regex(@"(\w)\1");
            var notStrings = new Regex(@"^((?!(ab|cd|pq|xy)).)*$");

            foreach (var str in input)
            {
                if (notStrings.IsMatch(str) &&
                    vowels.IsMatch(str) &&
                    doubleLetters.IsMatch(str))
                {
                    niceStrings++;
                }
            }

            return niceStrings.ToString();
        }

        public override string GetPartTwo()
        {
            int niceStrings = 0;
            var doubleLetters = new Regex(@"(\w\w).*\1");
            var repeatingLetters = new Regex(@"(\w).\1");

            foreach (var str in input)
            {
                if (doubleLetters.IsMatch(str) && repeatingLetters.IsMatch(str))
                {
                    niceStrings++;
                }
            }

            return niceStrings.ToString();
        }
    }
}
