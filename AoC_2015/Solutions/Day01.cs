﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day01 : BaseSolution
    {
        private readonly char[] input;

        public Day01() : base(2015, 1) => input = InputParser.FileToCharArray(FilePath);

        public override string GetPartOne()
        {
            int floor = 0;

            foreach (var i in input)
            {
                floor += i == '(' ? 1 : -1;
            }

            return floor.ToString();
        }

        public override string GetPartTwo()
        {
            int floor = 0;

            for (int i = 0; i < input.Length; i++)
            {
                floor += input[i] == '(' ? 1 : -1;
                if (floor < 0)
                {
                    return (i + 1).ToString();
                }
            }

            return String.Empty;
        }
    }
}
