﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2015.Solutions
{
    public class Day09 : BaseSolution
    {
        Dictionary<Tuple<string, string>, int> Locations = new();
        private List<string> AllTowns = new();

        public Day09() : base(2015, 9)
        {
            List<Match> lines = InputParser.FileToStringArray(FilePath)
                .Select(l => Regex.Match(l, @"(\w+) to (\w+) = (\d+)")).ToList();

            lines.ForEach(line =>
            {
                Locations[new(line.Groups[1].Value, line.Groups[2].Value)] = Convert.ToInt32(line.Groups[3].Value);
                Locations[new(line.Groups[2].Value, line.Groups[1].Value)] = Convert.ToInt32(line.Groups[3].Value);

                if (!AllTowns.Contains(line.Groups[1].Value))
                {
                    AllTowns.Add(line.Groups[1].Value);
                }
                if (!AllTowns.Contains(line.Groups[2].Value))
                {
                    AllTowns.Add(line.Groups[2].Value);
                }
            });
        }

        public static List<List<string>> BuildPermutations(List<string> items)
        {
            if (items.Count > 1)
            {
                return items.SelectMany(item => BuildPermutations(items.Where(i => !i.Equals(item)).ToList()),
                                       (item, permutation) => new[] { item }.Concat(permutation).ToList()).ToList();
            }

            return new List<List<string>> { items };
        }

        private string ProcessPermutations(bool isPartTwo = false)
        {
            long minTripLength = long.MaxValue;
            long maxTripLength = 0;

            List<List<string>> allPermutations = BuildPermutations(AllTowns);
            foreach (List<string> thisPermutation in allPermutations)
            {
                long tripLength = 0;
                for (int i = 0; i < thisPermutation.Count - 1; i++)
                    tripLength += Locations[new Tuple<string, string>(thisPermutation[i], thisPermutation[i + 1])];

                minTripLength = Math.Min(tripLength, minTripLength);
                maxTripLength = Math.Max(tripLength, maxTripLength);
            }

            return isPartTwo ? maxTripLength.ToString() : minTripLength.ToString();
        }


        public override string GetPartOne() => ProcessPermutations();

        public override string GetPartTwo() => ProcessPermutations(true);
    }
}
