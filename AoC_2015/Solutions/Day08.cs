﻿using AoC.Common;
using System.Text.RegularExpressions;

namespace AoC_2015.Solutions
{
    public class Day08 : BaseSolution
    {
        private readonly string[] input;

        public Day08() : base(2015, 8)
        {
            input = InputParser.FileToStringArray(FilePath);
        }

        public override string GetPartOne() => input.Sum(line => line.Length - Regex.Replace(line.Trim('"').Replace("\\\"", "_").Replace("\\\\", "_"), @"\\x\w{2}", "_").Length).ToString();

        public override string GetPartTwo() => input.Sum(w => w.Replace("\\", "__").Replace("\"", "__").Length + 2 - w.Length).ToString();

    }
}
