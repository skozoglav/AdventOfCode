﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day11 : BaseSolution
    {
        public Day11() : base(2015, 11)
        {
        }

        public override string GetPartOne() => ExecutePart("cqjxjnds");

        public override string GetPartTwo() => ExecutePart("cqjxxyzz");

        private static string ExecutePart(string input)
        {
            char[] password = input.ToCharArray();
            do
            {
                Increment(ref password);
            } while (!IsValid(ref password));
            return new(password);
        }

        private static bool IsValid(ref char[] password)
        {
            bool check1 = false;
            bool check3 = false;
            for (int j = 2; j < password.Length; j++)
            {
                //do check2 first because it returns
                if (password[j] == 'i' || password[j] == 'o' || password[j] == 'l') return false;
                if (password[j - 2] + 1 == password[j - 1] && password[j - 1] + 1 == password[j])
                {
                    check1 = true;
                }
                if (j <= 2) continue;
                for (int k = 0; k + 2 < j; k++)
                {
                    if (password[j - 3 - k] == password[j - 2 - k]
                        && password[j - 1] == password[j]
                        && password[j - 2 - k] != password[j - 1])
                    {
                        check3 = true;
                    }
                }
            }
            return check1 && check3;
        }

        private static void Increment(ref char[] password, int at = -1)
        {
            if (at == -1)
            {
                at = password.Length - 1;
            }
            password[at]++;
            if (password[at] == 'i' || password[at] == 'o' || password[at] == 'l') password[at]++;
            if (password[at] <= 'z') return;
            password[at] = 'a';
            Increment(ref password, at - 1);
        }
    }
}
