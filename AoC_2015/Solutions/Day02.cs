﻿using AoC.Common;

namespace AoC_2015.Solutions
{
    public class Day02 : BaseSolution
    {
        private readonly (int l, int w, int h)[] input;

        public Day02() : base(2015, 2)
        {
            // AxBxC => (int l, int w, int h) = A, B, C
            input = InputParser.FileToStringArray(FilePath).Select(x => x.Split('x'))
                .Select(x => (int.Parse(x[0]), int.Parse(x[1]), int.Parse(x[2]))).ToArray();
        }

        public override string GetPartOne()
        {
            int result = 0;

            foreach ((int l, int w, int h) in input)
            {
                result += ((2 * l * w) + (2 * w * h) + (2 * h * l));

                (int a, int b) = GetTwoSmallestSide(l, w, h);
                result += a * b;
            }

            return result.ToString();
        }

        public override string GetPartTwo()
        {
            int result = 0;

            foreach ((int l, int w, int h) in input)
            {
                result += (l * w * h);

                (int a, int b) = GetTwoSmallestSide(l, w, h);
                result += a + a + b + b;
            }

            return result.ToString();
        }

        private (int a, int b) GetTwoSmallestSide(int l, int w, int h)
        {
            if (l >= w && l >= h)
            {
                return (w, h);
            }
            else if (w >= l && w >= h)
            {
                return (l, h);
            }
            return (w, l);
        }
    }
}
